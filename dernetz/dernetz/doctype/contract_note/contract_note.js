// Copyright (c) 2020, Tech Innovators and contributors
// For license information, please see license.txt

/* ---------------------------------------------------------------------------
 * Doctype: Contract Note Calculations / Filters
 * --------------------------------------------------------------------------- */

frappe.ui.form.on('Contract Note', {

	 onload: function(frm) {
	   frm.trigger('contract_type')
	 },
	 validate: function(frm) {

	     if(frm.doc.contract_type == "MOP Contract"){
	        frm.trigger('total_for_mop_service')
            frm.trigger('mop_profit')
         }
         else if(frm.doc.contract_type == "Gas Contract"){
            frm.trigger('contract_cost_for_client')
            frm.trigger('sold_contract_start_date')
            frm.trigger('commission_amount')
         }
         else if(frm.doc.contract_type == "Electricity Contract"){

            if(frm.doc.uplift <= 0.01 || frm.doc.uplift >= 3)
            {
                frappe.throw("Uplift Number Should Be Between 0.01 Pence To 3 Pence.")
            }
            frm.trigger('sold_contract_start_date')
            frm.trigger('total_eac')
            frm.trigger('commission_amount')

//            if(frm.doc.commission_amount <= frm.doc.contract_cost_for_client)
//            {
//                frappe.throw("Commission Amount Should be Bigger Then Contract Cost.")
//            }

            if(frm.doc.mpan_topline.search("-") <= 0){
                var num = frm.doc.mpan_topline
                var parts = [num.slice(0,2),num.slice(2,5),num.slice(5,8)];
                //console.log(parts)
                var mpan_topline = parts[0]+"-"+parts[1]+"-"+parts[2];
                frm.set_value("mpan_topline",mpan_topline)
            }

            if(frm.doc.mpan_core.search("-") <= 0){
               var number = frm.doc.mpan_core
               var mpan = [number.slice(0,2),number.slice(2,10),number.slice(10,12)];
               //console.log(mpan)
               var mpan_core = mpan[0]+"-"+mpan[1]+"-"+mpan[2];
               //console.log(mpan_core)
               frm.set_value("mpan_core",mpan_core)
            }
         }


         // Clear The MOP Contract Fields If Contract Type Is Not MOP Contract
         /*if(frm.doc.contract_type != "MOP Contract"){
             frm.set_value("mop_site_address","")
             frm.set_value("mop_price","")
             frm.set_value("dc_price","")
             frm.set_value("da_price","")
             frm.set_value("mop_supplier_cost","")
         }*/

         // Clear The Water Contract Fields If Contract Type Is Not Water Contract
         /*if(frm.doc.contract_type != "Water Contract"){
             frm.set_value("water_site_address","")
             frm.set_value("water_consumption","")
             frm.set_value("waste_consumption","")
             frm.set_value("water_unit_rates","")
             frm.set_value("waste_unit_rates","")
             frm.set_value("water_fixed_rates","")
             frm.set_value("waste_fixed_rates","")
             frm.set_value("surface_drainage_fixed_rates","")
             frm.set_value("roads_drainage_fixed_rates","")
             frm.set_value("total_annual_wholesale_charge","")
         }*/

          // Clear The Electricity Contract Fields If Contract Type Is Not Electricity Contract
         /*if(frm.doc.contract_type != "Electricity Contract"){
             frm.set_value("mpan_site_address","")
             frm.set_value("mpan_topline","")
             frm.set_value("mpan_core","")
             frm.set_value("eac","")
             frm.set_value("current_supplier","")
             frm.set_value("new_supplier","")
             frm.set_value("current_contract_end_date","")
             frm.set_value("sold_contract_start_date","")
             frm.set_value("sold_contract_end_date","")
             frm.set_value("total_eac","")
             frm.set_value("day_eac_ussage","")
             frm.set_value("night_eac_ussage","")
             frm.set_value("day_rate_for_client","")
             frm.set_value("night_rate_for_client","")
             frm.set_value("standing_charge_for_client","")
             frm.set_value("kva_allocated","")
             frm.set_value("kva_charge_at","")
             frm.set_value("fit","")
             frm.set_value("ro","")
             frm.set_value("ccl","")
             frm.set_value("contract_cost_for_client","")
             frm.set_value("uplift","")
             frm.set_value("commission_amount","")
             //frm.set_value("loa","")
             //frm.set_value("signed_contract","")
         }*/

         /*if(frm.doc.contract_type != "Gas Contract"){
            frm.set_value("mprn","")
            frm.set_value("rate","")
         }*/

	 },
	 contract_type: function(frm) {

        if(frm.doc.contract_type == "Electricity Contract"){
           frm.toggle_display("mprn", false)
           frm.toggle_display("rate", false)
           frm.toggle_display("mpan_topline", true)
           frm.toggle_display("mpan_site_address", true)
           frm.toggle_display("mpan_core", true)
           frm.toggle_display("meter_type", true)
          // frm.toggle_display("total_eac", true)
           frm.toggle_display("day_eac_ussage", true)
           frm.toggle_display("night_eac_ussage", true)
           frm.toggle_display("day_rate_for_client", true)
           frm.toggle_display("night_rate_for_client", true)
           frm.toggle_display("kva_allocated", true)
           frm.toggle_display("kva_charge_at", true)
           //frm.toggle_display("fit", true)
           //frm.toggle_display("ro", true)
           //frm.toggle_display("ccl", true)
           frm.set_df_property("total_eac", "read_only", 1);

           /*frm.set_value("mprn","")
           frm.set_value("rate","")*/

           /*frm.set_value("mop_site_address","")
           frm.set_value("mop_price","")
           frm.set_value("dc_price","")
           frm.set_value("da_price","")
           frm.set_value("mop_supplier_cost","")

           frm.set_value("water_site_address","")
           frm.set_value("water_consumption","")
           frm.set_value("waste_consumption","")
           frm.set_value("water_unit_rates","")
           frm.set_value("waste_unit_rates","")
           frm.set_value("water_fixed_rates","")
           frm.set_value("waste_fixed_rates","")
           frm.set_value("surface_drainage_fixed_rates","")
           frm.set_value("roads_drainage_fixed_rates","")
           frm.set_value("total_annual_wholesale_charge","")*/
        }
        else if(frm.doc.contract_type == "Gas Contract"){
           frm.toggle_display("mprn", true)
           frm.toggle_display("rate", true)
           frm.toggle_display("rate", true)
           frm.toggle_display("mpan_topline", false)
           frm.toggle_display("mpan_site_address", false)
           frm.toggle_display("mpan_core", false)
           frm.toggle_display("meter_type", false)
          // frm.toggle_display("total_eac", false)
           frm.toggle_display("day_eac_ussage", false)
           frm.toggle_display("night_eac_ussage", false)
           frm.toggle_display("day_rate_for_client", false)
           frm.toggle_display("night_rate_for_client", false)
           frm.toggle_display("kva_allocated", false)
           frm.toggle_display("kva_charge_at", false)
           //frm.toggle_display("fit", false)
           //frm.toggle_display("ro", false)
           //frm.toggle_display("ccl", false)
           frm.set_df_property("total_eac", "read_only", 0);

           /*frm.set_value("mpan_site_address","")
           frm.set_value("mpan_topline","")
           frm.set_value("mpan_core","")
           frm.set_value("eac","")
           frm.set_value("current_supplier","")
           frm.set_value("new_supplier","")
           frm.set_value("current_contract_end_date","")
           frm.set_value("sold_contract_start_date","")
           frm.set_value("sold_contract_end_date","")
           frm.set_value("total_eac","")
           frm.set_value("day_eac_ussage","")
           frm.set_value("night_eac_ussage","")
           frm.set_value("day_rate_for_client","")
           frm.set_value("night_rate_for_client","")
           frm.set_value("standing_charge_for_client","")
           frm.set_value("kva_allocated","")
           frm.set_value("kva_charge_at","")
           frm.set_value("fit","")
           frm.set_value("ro","")
           frm.set_value("ccl","")
           frm.set_value("contract_cost_for_client","")
           frm.set_value("uplift","")
           frm.set_value("commission_amount","")*/

           /*frm.set_value("mop_site_address","")
           frm.set_value("mop_price","")
           frm.set_value("dc_price","")
           frm.set_value("da_price","")
           frm.set_value("mop_supplier_cost","")

           frm.set_value("water_site_address","")
           frm.set_value("water_consumption","")
           frm.set_value("waste_consumption","")
           frm.set_value("water_unit_rates","")
           frm.set_value("waste_unit_rates","")
           frm.set_value("water_fixed_rates","")
           frm.set_value("waste_fixed_rates","")
           frm.set_value("surface_drainage_fixed_rates","")
           frm.set_value("roads_drainage_fixed_rates","")
           frm.set_value("total_annual_wholesale_charge","")*/
        }
        /*else if(frm.doc.contract_type == "MOP Contract"){

           frm.set_value("water_site_address","")
           frm.set_value("water_consumption","")
           frm.set_value("waste_consumption","")
           frm.set_value("water_unit_rates","")
           frm.set_value("waste_unit_rates","")
           frm.set_value("water_fixed_rates","")
           frm.set_value("waste_fixed_rates","")
           frm.set_value("surface_drainage_fixed_rates","")
           frm.set_value("roads_drainage_fixed_rates","")
           frm.set_value("total_annual_wholesale_charge","")

           frm.set_value("mpan_site_address","")
           frm.set_value("mpan_topline","")
           frm.set_value("mpan_core","")
           frm.set_value("eac","")
           frm.set_value("current_supplier","")
           frm.set_value("new_supplier","")
           frm.set_value("current_contract_end_date","")
           frm.set_value("sold_contract_start_date","")
           frm.set_value("sold_contract_end_date","")
           frm.set_value("total_eac","")
           frm.set_value("day_eac_ussage","")
           frm.set_value("night_eac_ussage","")
           frm.set_value("day_rate_for_client","")
           frm.set_value("night_rate_for_client","")
           frm.set_value("standing_charge_for_client","")
           frm.set_value("kva_allocated","")
           frm.set_value("kva_charge_at","")
           frm.set_value("fit","")
           frm.set_value("ro","")
           frm.set_value("ccl","")
           frm.set_value("contract_cost_for_client","")
           frm.set_value("uplift","")
           frm.set_value("commission_amount","")

           frm.set_value("mprn","")
           frm.set_value("rate","")
        }
        else if(frm.doc.contract_type == "Water Contract"){

           frm.set_value("mprn","")
           frm.set_value("rate","")

           frm.set_value("mop_site_address","")
           frm.set_value("mop_price","")
           frm.set_value("dc_price","")
           frm.set_value("da_price","")
           frm.set_value("mop_supplier_cost","")

           frm.set_value("mpan_site_address","")
           frm.set_value("mpan_topline","")
           frm.set_value("mpan_core","")
           frm.set_value("eac","")
           frm.set_value("current_supplier","")
           frm.set_value("new_supplier","")
           frm.set_value("current_contract_end_date","")
           frm.set_value("sold_contract_start_date","")
           frm.set_value("sold_contract_end_date","")
           frm.set_value("total_eac","")
           frm.set_value("day_eac_ussage","")
           frm.set_value("night_eac_ussage","")
           frm.set_value("day_rate_for_client","")
           frm.set_value("night_rate_for_client","")
           frm.set_value("standing_charge_for_client","")
           frm.set_value("kva_allocated","")
           frm.set_value("kva_charge_at","")
           frm.set_value("fit","")
           frm.set_value("ro","")
           frm.set_value("ccl","")
           frm.set_value("contract_cost_for_client","")
           frm.set_value("uplift","")
           frm.set_value("commission_amount","")
        }*/
	 },
	 mop_price: function(frm) {
        frm.trigger('total_for_mop_service')
     },
     dc_price: function(frm) {
        frm.trigger('total_for_mop_service')
     },
     da_price: function(frm) {
        frm.trigger('total_for_mop_service')
     },
     total_for_mop_service: function(frm) {
        var total_for_mop_service = frm.doc.mop_price + frm.doc.dc_price + frm.doc.da_price
        frm.set_value("total_for_mop_service",total_for_mop_service)
        frm.trigger('mop_profit')
     },
     mop_supplier_cost: function(frm) {
        frm.trigger('mop_profit')
     },
     mop_profit: function(frm) {
        var mop_profit = frm.doc.total_for_mop_service - frm.doc.mop_supplier_cost
        frm.set_value("mop_profit",mop_profit)
     },
     eac: function(frm) {
        //if(frm.doc.contract_type == "Electricity Contract"){
        //frm.trigger('contract_cost_for_client')
        //frm.trigger('commission_amount')
        //}
     },
     total_eac: function(frm) {
        if(frm.doc.total_eac > 0){
            //var day_eac_ussage = frm.doc.total_eac * 0.7
            //var night_eac_ussage = frm.doc.total_eac * 0.3
            //frm.set_value("day_eac_ussage",day_eac_ussage)
            //frm.set_value("night_eac_ussage",night_eac_ussage)
            //frm.set_value("night_eac_ussage",night_eac_ussage)
            frm.trigger('contract_cost_for_client')
        }
        //frm.trigger('commission_amount')
    },
    day_eac_ussage: function(frm) {
        if(frm.doc.day_eac_ussage > 0){
            var total_eac = frm.doc.day_eac_ussage + frm.doc.night_eac_ussage
            frm.set_value("total_eac",total_eac)
            frm.trigger('contract_cost_for_client')
        }
    },
    night_eac_ussage: function(frm) {
        if(frm.doc.night_eac_ussage > 0){
            var total_eac = frm.doc.day_eac_ussage + frm.doc.night_eac_ussage
            frm.set_value("total_eac",total_eac)
            frm.trigger('contract_cost_for_client')
        }
    },
    sold_contract_start_date: function(frm) {
        var days = frm.doc.contract_period*365
        if(frm.doc.sold_contract_start_date && days){
            var sold_contract_end_date = frappe.datetime.add_days(frm.doc.sold_contract_start_date,days);
            frm.set_value("sold_contract_end_date", sold_contract_end_date);
            refresh_field("sold_contract_end_date");
        }
    },
    standing_charge_for_client: function(frm) {
        frm.trigger('contract_cost_for_client')
    },
   kva_charge_at: function(frm) {
        frm.trigger('contract_cost_for_client')
    },
    kva_allocated: function(frm) {
        frm.trigger('contract_cost_for_client')
   },
   ro: function(frm) {
       frm.trigger('contract_cost_for_client')
   },
    fit: function(frm) {
       frm.trigger('contract_cost_for_client')
    },
    day_rate_for_client: function(frm) {
        frm.trigger('contract_cost_for_client')
    },
    night_rate_for_client: function(frm) {
        frm.trigger('contract_cost_for_client')
    },
   ccl: function(frm) {
       frm.trigger('contract_cost_for_client')
    },
    contract_cost_for_client: function(frm) {
        if(frm.doc.contract_type == "Electricity Contract"){
            if(frm.doc.day_rate_for_client > 0 && frm.doc.day_eac_ussage > 0 && frm.doc.night_rate_for_client > 0
            && frm.doc.night_eac_ussage > 0 && frm.doc.standing_charge_for_client > 0 &&
            frm.doc.total_eac > 0 ) {
                var x1 = ( frm.doc.day_rate_for_client * frm.doc.day_eac_ussage )/ 100
                var x2 = ( frm.doc.night_rate_for_client * frm.doc.night_eac_ussage )/ 100
                var x3 = ( frm.doc.standing_charge_for_client * 365 )/ 100
                var x4 = ( frm.doc.kva_charge_at * frm.doc.kva_allocated * 365 )/ 100
                var x5 = ( frm.doc.ro / 100 ) * frm.doc.total_eac
                var x6 = ( frm.doc.fit / 100 ) * frm.doc.total_eac
                var x7 = ( frm.doc.total_eac * frm.doc.ccl )/ 100
                var contract_cost_for_client = ( x1 + x2 + x3 + x4 + x5 + x6 + x7 )
            }
        }
        else if(frm.doc.contract_type == "Gas Contract"){
            if(frm.doc.rate > 0 && frm.doc.standing_charge_for_client > 0 && frm.doc.total_eac > 0 ) {
                var x1 = ( frm.doc.total_eac / 100 )/ frm.doc.rate
                var x3 = ( frm.doc.standing_charge_for_client * 365 )/ 100
                var x5 = ( frm.doc.ro / 100 ) * frm.doc.total_eac
                var x6 = ( frm.doc.fit / 100 ) * frm.doc.total_eac
                var x7 = ( frm.doc.total_eac * frm.doc.ccl )/ 100
                var contract_cost_for_client = ( x1 + x3 + x5 + x6 + x7 )
           }
        }
        frm.set_value("contract_cost_for_client",contract_cost_for_client)
    },
    uplift: function(frm) {
        frm.trigger('commission_amount')
    },
    contract_period: function(frm) {
        frm.trigger('sold_contract_start_date')
        frm.trigger('commission_amount')
    },
    commission_amount: function(frm) {
        //if(frm.doc.contract_type == "Electricity Contract"){
        if(frm.doc.total_eac > 0 && frm.doc.uplift > 0 && frm.doc.contract_period > 0 ){
          var commission_amount = ( ( frm.doc.total_eac / 100 ) * frm.doc.uplift * frm.doc.contract_period )
          frm.set_value("commission_amount",commission_amount)
        }
    }
});
